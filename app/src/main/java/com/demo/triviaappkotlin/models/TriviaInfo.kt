package com.demo.triviaappkotlin.models

import android.os.Parcel
import android.os.Parcelable
import androidx.databinding.BaseObservable

data class TriviaInfo(var question : String,
var answer : String) : BaseObservable(),Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString()!!,
            parcel.readString()!!)

    constructor():this("","")

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(question)
        parcel.writeString(answer)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TriviaInfo> {
        override fun createFromParcel(parcel: Parcel): TriviaInfo {
            return TriviaInfo(parcel)
        }

        override fun newArray(size: Int): Array<TriviaInfo?> {
            return arrayOfNulls(size)
        }
    }
}





