package com.demo.triviaappkotlin.models

import android.os.Parcel
import android.os.Parcelable
import androidx.databinding.BaseObservable

class Trivia(
    var id: String, var userName: String,
    var listTriviaInfo: MutableList<TriviaInfo>?,
    var dateTime: String) : BaseObservable(), Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString()!!,
            parcel.readString()!!,
            parcel.createTypedArrayList(CREATOR)!!,
            parcel.readString()!!)

    constructor() : this("","", null,"")

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(userName)
        parcel.writeTypedList(listTriviaInfo)
        parcel.writeString(dateTime)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TriviaInfo> {
        override fun createFromParcel(parcel: Parcel): TriviaInfo {
            return TriviaInfo(parcel)
        }

        override fun newArray(size: Int): Array<TriviaInfo?> {
            return arrayOfNulls(size)
        }
    }
}