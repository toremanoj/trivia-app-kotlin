package com.demo.triviaappkotlin.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.demo.triviaappkotlin.R
import com.demo.triviaappkotlin.databinding.ViewSummaryBinding
import com.demo.triviaappkotlin.models.TriviaInfo

class TriviaInfoAdapter(private val list: MutableList<TriviaInfo>?) :
    RecyclerView.Adapter<TriviaInfoAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding: ViewSummaryBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.view_summary, parent, false
        )
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val triviaInfo: TriviaInfo? = list?.get(position)
        holder.binding.triviaInfo = triviaInfo
    }

    override fun getItemCount(): Int {
        return list?.size!!
    }

    inner class MyViewHolder(val binding: ViewSummaryBinding) :
        RecyclerView.ViewHolder(binding.root)

}
