package com.demo.triviaappkotlin.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.demo.triviaappkotlin.R
import com.demo.triviaappkotlin.databinding.ViewHistoryBinding
import com.demo.triviaappkotlin.models.Trivia

class TriviaHistoryAdapter(private val context: Context, private val list: List<Trivia>) :
    RecyclerView.Adapter<TriviaHistoryAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding: ViewHistoryBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.view_history, parent, false
        )
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val trivia: Trivia = list[position]
        holder.binding.trivia = trivia
        holder.binding.recyclerTriviaInfo.layoutManager =
            LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        val triviaInfoAdapter = TriviaInfoAdapter(trivia.listTriviaInfo)
        holder.binding.recyclerTriviaInfo.adapter = triviaInfoAdapter
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class MyViewHolder(val binding: ViewHistoryBinding) :
        RecyclerView.ViewHolder(binding.root)

}
