package com.demo.triviaappkotlin.database.triavia_data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface TriviaDao {
    @Insert
    fun insert(trivia: Trivia?): Long?

    @Query("SELECT * FROM tbl_trivia")
    fun fetchAll(): LiveData<List<Trivia?>?>?
}