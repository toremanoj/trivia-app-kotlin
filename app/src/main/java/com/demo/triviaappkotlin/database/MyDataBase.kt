package com.demo.triviaappkotlin.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.demo.triviaappkotlin.database.triavia_data.Trivia
import com.demo.triviaappkotlin.database.triavia_data.TriviaDao

@Database(entities = [Trivia::class], version = 1, exportSchema = false)
abstract class MyDataBase : RoomDatabase() {
    abstract fun triviaDao(): TriviaDao?
}
