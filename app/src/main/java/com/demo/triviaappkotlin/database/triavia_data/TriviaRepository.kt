package com.demo.triviaappkotlin.database.triavia_data

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.Room
import com.demo.triviaappkotlin.database.MyDataBase
import dagger.hilt.android.qualifiers.ApplicationContext
import java.util.concurrent.Executors
import javax.inject.Inject

class TriviaRepository @Inject constructor(@ApplicationContext context: Context?) {
    private val DB_NAME = "db_trivia"
    private val myDataBase: MyDataBase =
        Room.databaseBuilder(context!!, MyDataBase::class.java, DB_NAME).build()

    fun insertTrivia(trivia: Trivia?) {
        val executor = Executors.newSingleThreadExecutor()
        executor.execute { myDataBase.triviaDao()?.insert(trivia) }
    }

    val triviaHistory: LiveData<List<Trivia?>?>?
        get() = myDataBase.triviaDao()?.fetchAll()

}