package com.demo.triviaappkotlin.database.triavia_data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tbl_trivia")
class Trivia {
    @PrimaryKey(autoGenerate = true)
    var id = 0
    var userName: String? = null
    var triviaInfo: String? = null
    var dateTime: String? = null
}