package com.demo.triviaappkotlin.utils

import java.text.SimpleDateFormat
import java.util.*

class Utils {
    companion object {
        fun removeLastCharacter(str: String): String {
            var result: String? = null
            if (str.isNotEmpty()) {
                result = str.substring(0, str.length - 1)
            }
            return result!!
        }
        fun getCurrentTime(timeFormat: String?): String? {
            val c = Calendar.getInstance().time
            val df = SimpleDateFormat(timeFormat,Locale.getDefault())
            return df.format(c)
        }

        fun getCurrentDate(dateFormat: String?): String? {
            val c = Calendar.getInstance().time
            val df = SimpleDateFormat(dateFormat,Locale.getDefault())
            return df.format(c)
        }
    }
}