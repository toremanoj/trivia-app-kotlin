package com.demo.triviaappkotlin.ui.main.view_models

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.demo.triviaappkotlin.database.triavia_data.Trivia
import com.demo.triviaappkotlin.database.triavia_data.TriviaRepository
import com.demo.triviaappkotlin.utils.Utils
import com.google.gson.Gson
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class TriviaViewModel @Inject constructor(application: Application) :
    AndroidViewModel(application) {
    @Inject
    lateinit var triviaRepository: TriviaRepository

    //Insert information in local Database
    fun insertTrivia(trivia: com.demo.triviaappkotlin.models.Trivia) {
        val entityTrivia = Trivia()
        entityTrivia.dateTime =
            String.format(
                "%s %s", Utils.getCurrentDate("dd MMM"),
                Utils.getCurrentTime("hh:mm a")
        )
        entityTrivia.userName = trivia.userName
        entityTrivia.triviaInfo = Gson().toJson(trivia.listTriviaInfo)
        triviaRepository.insertTrivia(entityTrivia)
    }

    //Get list of all the games played
    val trivia: LiveData<List<Trivia?>?>?
        get() = triviaRepository.triviaHistory

}
