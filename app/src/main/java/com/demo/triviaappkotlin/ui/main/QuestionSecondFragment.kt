package com.demo.triviaappkotlin.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.demo.triviaappkotlin.R
import com.demo.triviaappkotlin.databinding.FragmentQuestionSecondBinding
import com.demo.triviaappkotlin.models.Trivia
import com.demo.triviaappkotlin.models.TriviaInfo
import com.demo.triviaappkotlin.utils.Utils
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class QuestionSecondFragment : Fragment() {
    private lateinit var binding: FragmentQuestionSecondBinding
    var triviaInfo: TriviaInfo? = null
    var trivia: Trivia? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_question_second,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Set binding variables
        binding.handler = ClickHandler()
        //Get trivia object from bundle
        trivia = QuestionSecondFragmentArgs.fromBundle(requireArguments()).trivia
        triviaInfo = TriviaInfo()
        triviaInfo!!.question = binding.txtquestion.text.toString()
        binding.triviaInfo = triviaInfo
    }

    // CLass to handle view click events
    inner class ClickHandler {
        fun onNextClick(view: View?) {
            var answer = ""
            //Check if user selected option
            if (binding.chkWhite.isChecked) {
                answer = String.format(
                    "%s %s,",
                    answer.trim { it <= ' ' },
                    binding.chkWhite.text.toString()
                )
            }
            if (binding.chkOrange.isChecked) {
                answer = String.format(
                    "%s %s,",
                    answer.trim { it <= ' ' },
                    binding.chkOrange.text.toString()
                )
            }
            if (binding.chkYellow.isChecked) {
                answer = String.format(
                    "%s %s,",
                    answer.trim { it <= ' ' },
                    binding.chkYellow.text.toString()
                )
            }
            if (binding.chkGreen.isChecked) {
                answer = String.format(
                    "%s %s,",
                    answer.trim { it <= ' ' },
                    binding.chkGreen.text.toString()
                )
            }
            //Check if user has answered question
            if (answer == "") {
                Toast.makeText(
                    requireActivity(),
                    resources.getString(R.string.err_select_answer),
                    Toast.LENGTH_SHORT
                ).show()
                return
            }
            //Remove last character (',' in this case) from answer given
            answer = Utils.removeLastCharacter(answer)
            //add trivia info in a list
            if (trivia?.listTriviaInfo != null) {
                triviaInfo!!.answer = answer
                trivia?.listTriviaInfo?.add(triviaInfo!!)
            }
            //Navigate to summary screen
            Navigation.findNavController(view!!)
                .navigate(
                    QuestionSecondFragmentDirections.actionQuestionSecondFragmentToSummaryFragment(
                        trivia!!
                    )
                )
        }
    }

}