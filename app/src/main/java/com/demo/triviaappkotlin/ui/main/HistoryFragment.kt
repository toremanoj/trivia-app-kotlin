package com.demo.triviaappkotlin.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.demo.triviaappkotlin.R
import com.demo.triviaappkotlin.adapters.TriviaHistoryAdapter
import com.demo.triviaappkotlin.databinding.FragmentHistoryBinding
import com.demo.triviaappkotlin.models.Trivia
import com.demo.triviaappkotlin.models.TriviaInfo
import com.demo.triviaappkotlin.ui.main.view_models.TriviaViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.hilt.android.AndroidEntryPoint
import java.lang.reflect.Type


@AndroidEntryPoint
class HistoryFragment : Fragment() {
    private lateinit var binding: FragmentHistoryBinding
    private var trivia: Trivia? = null
    private var listTrivia: MutableList<Trivia>? = null
    private var recyclerHistory: RecyclerView? = null
    private var triviaHistoryAdapter: TriviaHistoryAdapter? = null
    private var viewModel: TriviaViewModel? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_history, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Set binding variable
        binding.handler = ClickHandler()
        viewModel = ViewModelProvider(this).get(TriviaViewModel::class.java)
        getTriviaHistory()
        initRecyclerView()
    }

    // Get games history from database
    private fun getTriviaHistory() {
        viewModel!!.trivia!!.observe(
            viewLifecycleOwner,
            { trivias: List<com.demo.triviaappkotlin.database.triavia_data.Trivia?>? ->
                if (trivias != null && trivias.isNotEmpty()) {
                    listTrivia = mutableListOf()
                    binding.txtNoRecords.visibility = View.GONE
                    binding.recyclerHistory.visibility = View.VISIBLE
                    // Convert json string to List
                    val type: Type = object : TypeToken<MutableList<TriviaInfo?>?>() {}.type
                    for (tr in trivias) {
                        trivia = Trivia()
                        val triviaInfoList: MutableList<TriviaInfo> =
                            Gson().fromJson(tr?.triviaInfo, type)
                        trivia?.listTriviaInfo = triviaInfoList
                        trivia?.dateTime = tr?.dateTime!!
                        trivia?.userName = tr.userName!!
                        trivia?.id = tr.id.toString()
                        listTrivia?.add(trivia!!)
                    }
                    initAdapter()
                } else {
                    binding.txtNoRecords.visibility = View.VISIBLE
                    binding.recyclerHistory.visibility = View.GONE
                }
            })
    }

    //Initialize recycler adapter
    private fun initAdapter() {
        triviaHistoryAdapter = TriviaHistoryAdapter(requireActivity(), listTrivia!!)
        recyclerHistory!!.adapter = triviaHistoryAdapter
    }

    //Initialize recycler view
    private fun initRecyclerView() {
        recyclerHistory = binding.recyclerHistory
        recyclerHistory!!.layoutManager = LinearLayoutManager(
            activity,
            RecyclerView.VERTICAL,
            false
        )
    }

    // CLass to handle view click events
    class ClickHandler {
        fun onBackClick(view: View?) {
            Navigation.findNavController(view!!).popBackStack()
        }
    }

}