package com.demo.triviaappkotlin.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.demo.triviaappkotlin.R
import com.demo.triviaappkotlin.adapters.TriviaInfoAdapter
import com.demo.triviaappkotlin.databinding.FragmentSummaryBinding
import com.demo.triviaappkotlin.models.Trivia
import com.demo.triviaappkotlin.ui.main.view_models.TriviaViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SummaryFragment : Fragment() {
    private lateinit var binding: FragmentSummaryBinding
    var trivia: Trivia? = null

    private var recyclerTriviaInfo: RecyclerView? = null
    private var triviaInfoAdapter: TriviaInfoAdapter? = null
    var viewModel: TriviaViewModel? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_summary, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Set binding variables
        binding.handler = ClickHandler()
        //Initialize view model
        viewModel = ViewModelProvider(this)[TriviaViewModel::class.java]
        //get trivia object sent from previous screen
        trivia = SummaryFragmentArgs.fromBundle(requireArguments()).trivia
        binding.trivia = trivia
        initRecyclerView()
        initAdapter()
    }

    //Initialize trivia info adapter
    private fun initAdapter() {
        triviaInfoAdapter = TriviaInfoAdapter(trivia?.listTriviaInfo!!)
        recyclerTriviaInfo?.adapter = triviaInfoAdapter
    }

    //Initialize recycler view
    private fun initRecyclerView() {
        recyclerTriviaInfo = binding.recyclerTriviaInfo
        recyclerTriviaInfo?.layoutManager = LinearLayoutManager(
            activity,
            RecyclerView.VERTICAL,
            false
        )
    }

    // CLass to handle view click events
    inner class ClickHandler {
        fun onFinishClick(view: View?) {
            viewModel?.insertTrivia(trivia!!)
            //Return to home screen
            Navigation.findNavController(view!!)
                .navigate(SummaryFragmentDirections.actionSummaryFragmentToHomeFragment())
        }
    }

}