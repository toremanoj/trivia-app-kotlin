package com.demo.triviaappkotlin.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.demo.triviaappkotlin.R
import com.demo.triviaappkotlin.databinding.FragmentHomeBinding
import com.demo.triviaappkotlin.models.Trivia
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding
    lateinit var trivia: Trivia
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        trivia = Trivia()
        //Init binding variables
        binding.trivia = trivia
        binding.handler = ClickHandler()
    }


    // CLass to handle view click events
    inner class ClickHandler {
        fun onNextClick(view: View?) {
            //Check if user entered name or not
            if (trivia.userName == "") {
                binding.etName.requestFocus()
                binding.etName.error = getString(R.string.err_enter_name)
                return
            }
            // Navigate to next fragment
            Navigation.findNavController(view!!)
                .navigate(HomeFragmentDirections.actionHomeFragmentToQuestionFirstFragment(trivia))
        }

        // Show history of games played
        fun onHistoryClick(view: View?) {
            //Navigate to history
            Navigation.findNavController(view!!)
                .navigate(HomeFragmentDirections.actionHomeFragmentToHistoryFragment())
        }
    }
}