package com.demo.triviaappkotlin.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.demo.triviaappkotlin.R
import com.demo.triviaappkotlin.databinding.FragmentQuestionFirstBinding
import com.demo.triviaappkotlin.models.Trivia
import com.demo.triviaappkotlin.models.TriviaInfo
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class QuestionFirstFragment : Fragment() {
    private lateinit var binding: FragmentQuestionFirstBinding
    var triviaInfo: TriviaInfo? = null
    var trivia: Trivia? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_question_first,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Set binding variables
        binding.handler = ClickHandler()
        //Get trivia object from bundle
        trivia = QuestionFirstFragmentArgs.fromBundle(requireArguments()).trivia
        triviaInfo = TriviaInfo()
        triviaInfo!!.question = binding.txtquestion.text.toString()
        binding.triviaInfo = triviaInfo

        binding.radioGroup.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.radioSachin -> triviaInfo!!.answer = binding.radioSachin.text.toString()
                R.id.radioVirat -> triviaInfo!!.answer = binding.radioVirat.text.toString()
                R.id.radioAdam -> triviaInfo!!.answer = binding.radioAdam.text.toString()
                R.id.radioJacques -> triviaInfo!!.answer = binding.radioJacques.text.toString()
            }
        }
    }

    // CLass to handle view click events
    inner class ClickHandler {
        fun onNextClick(view: View?) {
            //Check if user has answered question
            if (triviaInfo!!.answer == "") {
                Toast.makeText(
                    activity,
                    resources.getString(R.string.err_select_answer),
                    Toast.LENGTH_SHORT
                ).show()
                return
            }
            //Create a list for trivia info and add elements in it
            val triviaInfoList: MutableList<TriviaInfo> = ArrayList<TriviaInfo>()
            triviaInfoList.add(triviaInfo!!)
            trivia!!.listTriviaInfo = triviaInfoList

            //Navigate to next question screen
            Navigation.findNavController(view!!)
                .navigate(
                    QuestionFirstFragmentDirections.actionQuestionFirstFragmentToQuestionSecondFragment(
                        trivia
                    )
                )
        }
    }

}